package hello;

public class Calculadora {
	
	public static Double somar(Double d1, Double d2) {
		return Double.sum(d1, d2);
	}
	
	public static Double multiplicar(Double d1, Double d2) {
		return d1 * d2;
	}

}
