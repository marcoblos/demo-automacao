package hello;

import org.junit.Assert;
import org.junit.Test;

public class CalculadoraTest {
	
	@Test
	public void testaMetodoSalvar() {
		// Arrange
		Double d1 = new Double(10);
		Double d2 = new Double(7);
		
		// Act
		Double resultado = Calculadora.somar(d1, d2);
		
		// Assert
		Assert.assertTrue(resultado.equals(new Double(17)));
	}
	
	@Test
	public void testaMetodoMultiplicar() {
		// Arrange
		Double d1 = new Double(10);
		Double d2 = new Double(7);
		
		// Act
		Double resultado = Calculadora.multiplicar(d1, d2);
		
		// Assert
		Assert.assertTrue(resultado.equals(new Double(70)));
	}

}
